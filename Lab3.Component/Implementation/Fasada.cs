﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Fasada:IZasilanie,IGrzanie,IMechanizm
    {
        public IZasilanie zasilacz = new Zasilacz();
        private IGrzanie megatron = new Megatron();
        private IMechanizm drzwi = new Drzwi();


        public Fasada()
        {

        }
  

        public bool CzyWpietyDoPradu()
        {
        
            return zasilacz.CzyWpietyDoPradu();
        }

        public void Zasilaj()
        {
            zasilacz.Zasilaj();
        }

        public bool SprawdzZwarcie()
        {
            return zasilacz.SprawdzZwarcie();
        }

        public void Podgrzej(decimal ileStopni, DateTime koniecGrzania)
        {
            megatron.Podgrzej(ileStopni, koniecGrzania);
        }

        public void PrzerwijGrzanie()
        {
            megatron.PrzerwijGrzanie();
        }

        public void OtworzDrzwi()
        {
            drzwi.OtworzDrzwi();
        }

        public void ZamknijDrzwi()
        {
            drzwi.ZamknijDrzwi();
        }

        public bool CzyOtwarte()
        {
            return drzwi.CzyOtwarte();
        }
    }
}
