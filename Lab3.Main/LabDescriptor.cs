﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1
        
        public static Type I1 = typeof(IGrzanie);
        public static Type I2 = typeof(IZasilanie);
        public static Type I3 = typeof(IMechanizm);

        public static Type Component = typeof(Fasada);

        public static GetInstance GetInstanceOfI1 = null;
        public static GetInstance GetInstanceOfI2 = null;
        public static GetInstance GetInstanceOfI3 = null;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Dzialanie);
        public static Type MixinFor = typeof(IGrzanie);

        #endregion
    }
}
